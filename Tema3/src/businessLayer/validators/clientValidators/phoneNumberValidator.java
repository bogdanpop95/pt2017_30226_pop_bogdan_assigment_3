package businessLayer.validators.clientValidators;

import java.util.regex.Pattern;
import model.Client;
import businessLayer.validators.Validator;

public class phoneNumberValidator implements Validator<Client>{
	
	private static final String PHONE_PATTERN = "^[0-9\\-\\+]{9,15}?";
	
	public void validate(Client t) {
		Pattern pattern = Pattern.compile(PHONE_PATTERN);
		if (!pattern.matcher(t.getPhoneNumber()).matches()) {
			throw new IllegalArgumentException("Invalid phoneNumber!");
		}
	}
}
