package businessLayer.validators.productValidators;

import model.Product;
import businessLayer.validators.Validator;

public class quantityValidator implements Validator<Product>{
	
	public void validate(Product t) {
		if (t.getQuantity() < 0)
			throw new IllegalArgumentException("Invalid product quantity (it must be a positive)!");
	}
}
