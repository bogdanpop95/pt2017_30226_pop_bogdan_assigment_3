package businessLayer.validators.productValidators;

import model.Product;
import businessLayer.validators.Validator;

public class priceValidator implements Validator<Product>{
	
	public void validate(Product t) {
		if (t.getPrice() < 0)
			throw new IllegalArgumentException("Invalid product price (it must be a positive)!");
	}
}
