package businessLayer.validators.orderValidators;

import model.Order;
import businessLayer.validators.Validator;

public class desiredQuantityValidator implements Validator<Order>{
	
	public void validate(Order t) {
		if (t.getProduct().getQuantity() == 0)
			throw new IllegalArgumentException("Product stock indisponible!");
		else if (t.getDesiredQuantity() <= 0 || t.getDesiredQuantity() > t.getProduct().getQuantity())
			throw new IllegalArgumentException("Invalid desired quantity!");
	}

}
