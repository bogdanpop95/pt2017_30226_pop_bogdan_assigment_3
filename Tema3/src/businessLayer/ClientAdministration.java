package businessLayer;

import java.util.*;

import businessLayer.validators.Validator;
import businessLayer.validators.clientValidators.*;
import model.Client;
import dataAccessLayer.dao.ClientDAO;

/**
 * @author pop_b
 * Clasa front-end a administrarii clientiilor si a operatiilor asupra acestora
 * Se folosesete de obiectul de tip DAO, ClientDAO pentru implementarea operatiilor si in plus valideaza datele care vor ajunge in tabele bazei de date
 * Metodele sunt identice cu cele din DAO, dar in plus sunt facute validarile
 */
public class ClientAdministration {
	
	private List<Validator<Client>> validators;
	private ClientDAO cDAO;
	
	public ClientAdministration(ClientDAO cDAO) {
		this.cDAO = cDAO;
		validators = new ArrayList<Validator<Client>>();
		validators.add(new NameValidator());
		validators.add(new phoneNumberValidator());
		validators.add(new EmailValidator());
	}
	
	public int insertClient(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return cDAO.insert(client);
	}
	
	public void editClient(Client client, String columnName, String newValue) {
		cDAO.edit(client, columnName, newValue);
		client = cDAO.findById(client.getIdClient());
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
	}
	
	public void deleteClient(Client client) {
		cDAO.delete(client);
	}
	
	public Client findClientById(int idClient) {
		Client cl = cDAO.findById(idClient);
		
		if (cl == null) {
			throw new NoSuchElementException("The client with id = " + idClient + " not found!");
		}
		return cl;
	}
	
	public List<Client> findAllClients() {
		List<Client> ls = cDAO.findAll();
		
		if(ls.isEmpty()) {
			System.out.println("The table is empty");
		}
		return ls;
	}
}
