package businessLayer;

import java.util.*;

import businessLayer.validators.Validator;
import businessLayer.validators.orderValidators.*;
import model.Order;
import model.Client;
import model.Product;
import dataAccessLayer.dao.OrderDAO;

/**
 * @author pop_b
 * Actioneaza asupra tabelei de comenzi, dar si asupra tabelei de produse atunci cand se face o operatie din OrderDAO
 * Procesarea comenziilor
 */
public class OrderProcessing {
	
	private Validator<Order> validator;
	private ClientAdministration cAdmin;
	private WarehouseAdministration wAdmin;
	
	public OrderProcessing(ClientAdministration cAdmin, WarehouseAdministration wAdmin) {
		this.cAdmin = cAdmin;
		this.wAdmin = wAdmin;
		validator = new desiredQuantityValidator();
	}
	
	public int createOrder(int idClient, int idProduct, int desiredQuantity) {
		Client client = cAdmin.findClientById(idClient);
		Product product = wAdmin.findProductById(idProduct);
		Order order = new Order(product, client, desiredQuantity, new Date());
		validator.validate(order);
		int dif = product.getQuantity() - desiredQuantity;
		wAdmin.editProduct(product, "quantity", dif);
		return OrderDAO.insert(order);
	}
	
	public void editOrder(Order order, String columnName, Object newValue) {
		Order ord = order;
		OrderDAO.edit(order, columnName, newValue);
		order = OrderDAO.findById(order.getID());
		validator.validate(order);
		// Reactualizam Product
		if (order.getDesiredQuantity() != ord.getDesiredQuantity()) {
			int sum = order.getDesiredQuantity() - ord.getDesiredQuantity();
			Product product = wAdmin.findProductById(order.getProduct().getIdProduct());
			wAdmin.editProduct(product, order.getProduct().getName(), product.getIdProduct() + sum);
		}
	}
	
	public void deleteOrder(Order order) {
		Product product = wAdmin.findProductById(order.getProduct().getIdProduct());
		int sum = product.getQuantity() + order.getDesiredQuantity();
		int row = OrderDAO.delete(order);
		
		if (row > -1) {
			System.out.println("Order(idOrder = " + order.getID() + " deleted from row( " + row + " )");
			wAdmin.editProduct(product, "quantity", sum);
		}
	}
	
	public Order findOrderById(int idOrder) {
		Order ord = OrderDAO.findById(idOrder);
		
		if (ord == null) {
			throw new NoSuchElementException("The order with id =" + idOrder + "not found!");
		}
		return ord;
	}
	
	public List<Order> findAllOrders() {
		List<Order> ls = OrderDAO.findAll();
		
		if(ls.isEmpty()) {
			System.out.println("The table is empty");
		}
		return ls;
	}
}
