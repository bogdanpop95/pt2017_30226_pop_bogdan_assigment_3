package businessLayer;

import java.util.*;

import businessLayer.validators.Validator;
import businessLayer.validators.productValidators.*;
import model.Product;
import dataAccessLayer.dao.ProductDAO;;

/**
 * @author pop_b
 * Clasa front-end a administrarii clientiilor si a operatiilor asupra acestora
 * Se folosesete de obiectul de tip DAO, ProductDAO pentru implementarea operatiilor si in plus valideaza datele care vor ajunge in tabele bazei de date
 * Metodele sunt identice cu cele din DAO, dar in plus sunt facute validarile
 */
public class WarehouseAdministration {
	
	private List<Validator<Product>> validators;
	private ProductDAO pDAO;
	
	public WarehouseAdministration(ProductDAO pDAO) {
		this.pDAO = pDAO;
		validators = new ArrayList<Validator<Product>>();
		validators.add(new priceValidator());
		validators.add(new quantityValidator());
	}
	
	public int insertProduct(Product product) {
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		return pDAO.insert(product);
	}
	
	public void editProduct(Product product, String columnName, Object newValue) {
		pDAO.edit(product, columnName, newValue);
		product = pDAO.findById(product.getIdProduct());
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
	}
	
	public void deleteProduct(Product product) {
		pDAO.delete(product);
		
	}
	
	public Product findProductById(int idProduct) {
		Product pr = pDAO.findById(idProduct);
		
		if (pr == null) {
			throw new NoSuchElementException("The product with id =" + idProduct + "not found!");
		}
		return pr;
	}
	
	public List<Product> findAllProducts() {
		List<Product> ls = pDAO.findAll();
		
		if(ls.isEmpty()) {
			System.out.println("The table is empty");
		}
		return ls;
	}
}
