package presentation;

import java.util.*;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import reflection.Reflection;
import model.Product;
import businessLayer.WarehouseAdministration;
import dataAccessLayer.dao.ProductDAO;;

/**
 * @author pop_b
 * Parte a GUI
 * View-ul care reprezinta tabela de produse
 */
public class ProductsView implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JLabel jlbName, jlbPrice, jlbQuantity;
	JTextField jtfName, jtfPrice, jtfQuantity, jtfFindById;
	JButton jbAdd, jbEdit, jbDelete, jbFindById, jbMenu;
	JScrollPane js;
	JTable jTable;
	List<Product> products;
	Object[][] line;
	WarehouseAdministration wAdmin;
	List<Object> values;
	Object[] data;
	Meniu menu;
	
	public ProductsView(Meniu menu) {
		this.menu = menu;
		prepareGUI();
		update();
	}
	
	/**
	 * Metoda ajuta la actualizarea(initializarea) Jtable-ului dupa o operatie
	 */
	private void update() {
		products = wAdmin.findAllProducts();
		line = new Object[products.size()][4];
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 700, 300);
		int i = 0, j;
		for (Product pr : products) {
			j = 0;
			values = Reflection.retrieveProprieties(pr);
			for (Object obj : values) {
				line[i][j++] = obj;
			}
			i++;
		}
		String[] columns = {"idProduct", "name", "price", "quantity"};
		jTable = new JTable(line, columns);
		js.setViewportView(jTable);
		js.repaint();
		jTable.repaint();
		cPane.add(js);
	}
	
	/**
	 * Initializare frame si instantiarea unui obiect WarehouseAdministration care are ca functionalitate managementul produselor, operatile pe tabela de produse
	 */
	private void prepareGUI() {
		wAdmin = new WarehouseAdministration(new ProductDAO());
		appFrame = new JFrame("ProductsView");
		appFrame.setSize(900, 600);
		appFrame.setLocation(500, 200);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	/**
	 * Aranjarea componentelor din frame (ContentPane)
	 */
	private void arrangeComponents() {
		jlbName = new JLabel("name");
		jlbName.setBounds(80, 400, 110, 30);
		jlbName.setFont(jlbName.getFont().deriveFont(16f));
		cPane.add(jlbName);
		
		jlbPrice = new JLabel("price");
		jlbPrice.setBounds(270, 400, 110, 30);
		jlbPrice.setFont(jlbPrice.getFont().deriveFont(16f));
		cPane.add(jlbPrice);
		
		jlbQuantity = new JLabel("quantity");
		jlbQuantity.setBounds(475, 400, 110, 30);
		jlbQuantity.setFont(jlbQuantity.getFont().deriveFont(16f));
		cPane.add(jlbQuantity);
		
		jtfName = new JTextField("");
		jtfName.setBounds(30, 440, 140, 30);
		jtfName.setFont(jtfName.getFont().deriveFont(16f));;
		cPane.add(jtfName);
		
		jtfPrice = new JTextField("");
		jtfPrice.setBounds(235, 440, 140, 30);
		jtfPrice.setFont(jtfPrice.getFont().deriveFont(16f));;
		cPane.add(jtfPrice);
		
		jtfQuantity = new JTextField("");
		jtfQuantity.setBounds(440, 440, 140, 30);
		jtfQuantity.setFont(jtfQuantity.getFont().deriveFont(16f));;
		cPane.add(jtfQuantity);
		
		jbAdd = new JButton("Add");
		jbAdd.setBounds(780, 430, 90, 40);
		jbAdd.setFont(jbAdd.getFont().deriveFont(16f));
		jbAdd.addActionListener(this);
		cPane.add(jbAdd);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(780, 200, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(780, 310, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
		
		jbMenu = new JButton("Meniu");
		jbMenu.setBounds(400, 500, 100, 50);
		jbMenu.setFont(jbMenu.getFont().deriveFont(18f));
		jbMenu.addActionListener(this);
		cPane.add(jbMenu);

	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbAdd) {
			Product product = new Product(jtfName.getText(), Double.parseDouble(jtfPrice.getText()), Integer.parseInt(jtfQuantity.getText()));
			wAdmin.insertProduct(product);
			jtfName.setText(""); jtfPrice.setText(""); jtfQuantity.setText("");
			update();
			System.gc();
		} else if (e.getSource() == jbEdit) {
			int row = jTable.getSelectedRow();
			int columns = jTable.getColumnCount();
			System.out.println((int)jTable.getValueAt(row, 0));
			Product product = wAdmin.findProductById((int)jTable.getValueAt(row, 0));
			data = new Object[columns];
			for (int i = 1; i < columns; i++) {
				data[i] = jTable.getValueAt(row, i);
				System.out.println(data[i]);
			}
			for (int i = 1; i < columns; i++) {
				System.out.println(jTable.getColumnName(i));
				wAdmin.editProduct(product, jTable.getColumnName(i), data[i]);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbDelete) {
			int row = jTable.getSelectedRow();
			System.out.println((int)jTable.getValueAt(row, 0));
			Product product = wAdmin.findProductById((int)jTable.getValueAt(row, 0));
			wAdmin.deleteProduct(product);
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			Product product = wAdmin.findProductById(Integer.parseInt(jtfFindById.getText()));
			for (int i = 0; i < jTable.getRowCount(); i++) {
				if ((int)jTable.getValueAt(i, 0) == product.getIdProduct()) {
					jTable.setRowSelectionInterval(i, i);
				}
			}
			jtfFindById.setText("");
		} else if (e.getSource() == jbMenu) {
			appFrame.setVisible(false);
			menu.appFrame.setVisible(true);
		}
	}

}
