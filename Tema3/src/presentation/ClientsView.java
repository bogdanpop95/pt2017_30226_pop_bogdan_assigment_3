package presentation;

import java.util.*;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import reflection.Reflection;
import model.Client;
import businessLayer.ClientAdministration;
import dataAccessLayer.dao.ClientDAO;

/**
 * @author pop_b
 * Parte a GUI
 * View-ul care reprezinta tabela de clienti
 */
public class ClientsView implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JLabel jlbName, jlbEmail, jlbAddress, jlbPhone;
	JTextField jtfName, jtfEmail, jtfAddress, jtfPhone, jtfFindById;
	JButton jbAdd, jbEdit, jbDelete, jbFindById, jbMenu;
	JScrollPane js;
	JTable jTable;
	List<Client> clients;
	Object[][] line;
	ClientAdministration cAdmin;
	List<Object> values;
	Object[] data;
	Meniu menu;
	
	public ClientsView(Meniu menu) {
		this.menu = menu;
		prepareGUI();
		update();
	}
	
	/**
	 * Metoda ajuta la actualizarea(initializarea) JTable-ului dupa o operatie
	 */
	private void update() {
		clients = cAdmin.findAllClients();
		line = new Object[clients.size()][5];
		if (js != null)
			cPane.remove(js);
		if (jTable != null)
			cPane.remove(jTable);
		cPane.revalidate();
		js = new JScrollPane();
		js.setBounds(50, 50, 700, 300);
		int i = 0, j;
		for (Client cl : clients) {
			j = 0;
			values = Reflection.retrieveProprieties(cl);
			for (Object obj : values) {
				line[i][j++] = obj;
			}
			i++;
		}
		String[] columns = {"idClient", "name", "phoneNumber", "address", "email"};
		jTable = new JTable(line, columns);
		js.setViewportView(jTable);
		js.repaint();
		jTable.repaint();
		cPane.add(js);
	}
	
	/**
	 * initializare frame si instantiarea unui obiect ClientAdministration care are ca functionalitate managementul clientilor, operatiile pe tabela de clienti
	 */
	private void prepareGUI() {
		cAdmin = new ClientAdministration(new ClientDAO());
		appFrame = new JFrame("ClientsView");
		appFrame.setSize(900, 600);
		appFrame.setLocation(500, 200);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	/**
	 * Aranjarea componentelor din frame (ContentPane)
	 */
	private void arrangeComponents() {
		jlbName = new JLabel("name");
		jlbName.setBounds(80, 400, 110, 30);
		jlbName.setFont(jlbName.getFont().deriveFont(16f));
		cPane.add(jlbName);
		
		jlbPhone = new JLabel("phoneNumber");
		jlbPhone.setBounds(200, 400, 110, 30);
		jlbPhone.setFont(jlbPhone.getFont().deriveFont(16f));
		cPane.add(jlbPhone);
		
		jlbAddress = new JLabel("address");
		jlbAddress.setBounds(375, 400, 110, 30);
		jlbAddress.setFont(jlbAddress.getFont().deriveFont(16f));
		cPane.add(jlbAddress);
		
		jlbEmail = new JLabel("email");
		jlbEmail.setBounds(540, 400, 110, 30);
		jlbEmail.setFont(jlbEmail.getFont().deriveFont(16f));
		cPane.add(jlbEmail);
		
		jtfName = new JTextField("");
		jtfName.setBounds(30, 440, 140, 30);
		jtfName.setFont(jtfName.getFont().deriveFont(16f));;
		cPane.add(jtfName);
		
		jtfPhone = new JTextField("");
		jtfPhone.setBounds(185, 440, 140, 30);
		jtfPhone.setFont(jtfPhone.getFont().deriveFont(16f));;
		cPane.add(jtfPhone);
		
		jtfAddress = new JTextField("");
		jtfAddress.setBounds(340, 440, 140, 30);
		jtfAddress.setFont(jtfAddress.getFont().deriveFont(16f));;
		cPane.add(jtfAddress);
		
		jtfEmail = new JTextField("");
		jtfEmail.setBounds(495, 440, 140, 30);
		jtfEmail.setFont(jtfEmail.getFont().deriveFont(16f));;
		cPane.add(jtfEmail);
		
		jbAdd = new JButton("Add");
		jbAdd.setBounds(780, 430, 90, 40);
		jbAdd.setFont(jbAdd.getFont().deriveFont(16f));
		jbAdd.addActionListener(this);
		cPane.add(jbAdd);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(780, 200, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(780, 310, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(780, 50, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(780, 100, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
		
		jbMenu = new JButton("Meniu");
		jbMenu.setBounds(400, 500, 100, 50);
		jbMenu.setFont(jbMenu.getFont().deriveFont(18f));
		jbMenu.addActionListener(this);
		cPane.add(jbMenu);
			
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbAdd) {
			Client client = new Client(jtfName.getText(), jtfPhone.getText(), jtfAddress.getText(), jtfEmail.getText());
			cAdmin.insertClient(client);
			jtfName.setText(""); jtfPhone.setText(""); jtfAddress.setText(""); jtfEmail.setText("");
			update();
			System.gc();
		} else if (e.getSource() == jbEdit) {
			int row = jTable.getSelectedRow();
			int columns = jTable.getColumnCount();
			System.out.println((int)jTable.getValueAt(row, 0));
			Client client = cAdmin.findClientById((int)jTable.getValueAt(row, 0));
			data = new Object[columns];
			for (int i = 1; i < columns; i++) {
				data[i] = (String)jTable.getValueAt(row, i);
				System.out.println(data[i]);
			}
			for (int i = 1; i < columns; i++) {
				System.out.println(jTable.getColumnName(i));
				cAdmin.editClient(client, jTable.getColumnName(i), data[i].toString());
			}
			update();
			System.gc();
		} else if (e.getSource() == jbDelete) {
			int row = jTable.getSelectedRow();
			System.out.println((int)jTable.getValueAt(row, 0));
			Client client = cAdmin.findClientById((int)jTable.getValueAt(row, 0));
			cAdmin.deleteClient(client);
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			Client client = cAdmin.findClientById(Integer.parseInt(jtfFindById.getText()));
			for (int i = 0; i < jTable.getRowCount(); i++) {
				if ((int)jTable.getValueAt(i, 0) == client.getIdClient()) {
					jTable.setRowSelectionInterval(i, i);
				}
			}
			jtfFindById.setText("");
		} else if (e.getSource() == jbMenu) {
			appFrame.setVisible(false);
			menu.appFrame.setVisible(true);
		}
	}
	

}
