package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import businessLayer.ClientAdministration;
import businessLayer.OrderProcessing;
import businessLayer.WarehouseAdministration;
import dataAccessLayer.dao.ClientDAO;
import dataAccessLayer.dao.OrderDAO;
import dataAccessLayer.dao.ProductDAO;
import model.Client;
import model.Product;
import reflection.Reflection;
import model.Order;

/**
 * @author pop_b
 * Parte a GUI
 * View-ul care reprezinta tabela de comenzi si care ne permite selectarea clientului si produsului care creeaza o comanda
 */
public class OrdersView implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JLabel jlbQuantity;
	JButton jbCreate, jbEdit, jbDelete, jbFindById, jbMenu, jbPrint;
	JTextField jtfFindById, jtfQuantity;
	JScrollPane js1, js2, js3;
	JTable jTable1, jTable2, jTable3;
	List<Client> clients;
	List<Product> products;
	List<Order> orders;
	Object[][] line1, line2, line3;
	ClientAdministration cAdmin;
	WarehouseAdministration wAdmin;
	OrderProcessing oAdmin;
	List<Object> values1, values2, values3;
	Object[] data;
	Meniu menu;
	
	public OrdersView(Meniu menu) {
		this.menu = menu;
		prepareGUI();
		update();
	}
	
	/**
	 * Metoda ajuta la acutalizarea(initializarea) tabelei de comenzi si produse dupa fiecare operatie
	 */
	private void update() {
		orders = oAdmin.findAllOrders();
		line3 = new Object[orders.size()][8];
		if (js3 != null)
			cPane.remove(js3);
		if (jTable3 != null) 
			cPane.remove(jTable3);
		cPane.revalidate();
		js3 = new JScrollPane();
		js3.setBounds(190, 320, 1000, 500);
		int i = 0, j;
		for (Order or : orders) {
			j = 0;
			values3 = Reflection.retrieveProprieties(or);
			for (Object obj : values3) {
				if (j < 8)
					line3[i][j++] = obj;
			}
			i++;
		}
		String[] columns = {"idOrder","idProduct", "productName", "idClient", "clientName", "quantity", "dated", "totalPrice"};
		jTable3 = new JTable(line3, columns);
		js3.setViewportView(jTable3);
		js3.repaint();
		jTable3.repaint();
		cPane.add(js3);
		// Tabela Produse
		products = wAdmin.findAllProducts();
		line2 = new Object[products.size()][4];
		i = 0;
		for (Product pr : products) {
			j = 0;
			values2 = Reflection.retrieveProprieties(pr);
			for (Object obj : values2) {
				line2[i][j++] = obj;
			}
			i++;
		}
		String[] columns2 = {"idProduct", "name", "price", "quantity"};
		jTable2 = new JTable(line2, columns2);
		jTable2.setDefaultEditor(Object.class, null);
		js2.setViewportView(jTable2);
		cPane.add(js2);
	}
	
	/**
	 * initializare frame si obiecte necesare pentru: administrare clienti, produse si comenzi (permit operatii)
	 */
	private void prepareGUI() {
		ClientDAO cDAO = new ClientDAO();
		ProductDAO pDAO = new ProductDAO();
		OrderDAO.cDAO = cDAO;
		OrderDAO.pDAO = pDAO;
		cAdmin = new ClientAdministration(cDAO);
		wAdmin = new WarehouseAdministration(pDAO);
		oAdmin = new OrderProcessing(cAdmin, wAdmin);
		
		appFrame = new JFrame("OrdersView");
		appFrame.setSize(1400, 1000);
		appFrame.setLocation(300, 20);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	/**
	 * Aranjarea componentelor din frame (ContentPane)
	 */
	private void arrangeComponents() {
		clients = cAdmin.findAllClients();
		products = wAdmin.findAllProducts();
		js1 = new JScrollPane();
		js2 = new JScrollPane();
		js1.setBounds(10, 10, 670, 300);
		js2.setBounds(690, 10, 670, 300);
		// Tabela Clienti
		line1 = new Object[clients.size()][5];
		int i = 0, j;
		for (Client cl : clients) {
			j = 0;
			values1 = Reflection.retrieveProprieties(cl);
			for (Object obj : values1) {
				line1[i][j++] = obj;
			}
			i++;
		}
		String[] columns1 = {"idClient", "name", "phoneNumber", "address", "email"};
		jTable1 = new JTable(line1, columns1);
		jTable1.setDefaultEditor(Object.class, null);
		js1.setViewportView(jTable1);
		cPane.add(js1);
		
		jbMenu = new JButton("Meniu");
		jbMenu.setBounds(650, 900, 100, 50);
		jbMenu.setFont(jbMenu.getFont().deriveFont(18f));
		jbMenu.addActionListener(this);
		cPane.add(jbMenu);
		
		jlbQuantity = new JLabel("quantity");
		jlbQuantity.setBounds(55, 500, 110, 30);
		jlbQuantity.setFont(jlbQuantity.getFont().deriveFont(16f));
		cPane.add(jlbQuantity);
		
		jtfQuantity = new JTextField("");
		jtfQuantity.setBounds(20, 540, 140, 30);
		jtfQuantity.setFont(jtfQuantity.getFont().deriveFont(16f));;
		cPane.add(jtfQuantity);
		
		jbCreate = new JButton("Create");
		jbCreate.setBounds(40, 400, 90, 40);
		jbCreate.setFont(jbCreate.getFont().deriveFont(16f));
		jbCreate.addActionListener(this);
		cPane.add(jbCreate);
		
		jbEdit = new JButton("Edit");
		jbEdit.setBounds(1200, 600, 90, 40);
		jbEdit.setFont(jbEdit.getFont().deriveFont(16f));
		jbEdit.addActionListener(this);
		cPane.add(jbEdit);
		
		jbDelete = new JButton("Delete");
		jbDelete.setBounds(1200, 780, 90, 40);
		jbDelete.setFont(jbDelete.getFont().deriveFont(16f));
		jbDelete.addActionListener(this);
		cPane.add(jbDelete);
		
		jbFindById = new JButton("Find Id");
		jbFindById.setBounds(1200, 350, 90, 40);
		jbFindById.setFont(jbFindById.getFont().deriveFont(16f));
		jbFindById.addActionListener(this);
		cPane.add(jbFindById);
		
		jbPrint = new JButton("Print");
		jbPrint.setBounds(40, 780, 90, 40);
		jbPrint.setFont(jbPrint.getFont().deriveFont(16f));
		jbPrint.addActionListener(this);
		cPane.add(jbPrint);
		
		jtfFindById = new JTextField("");
		jtfFindById.setBounds(1200, 400, 90, 30);
		jtfFindById.setFont(jtfFindById.getFont().deriveFont(16f));;
		cPane.add(jtfFindById);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbMenu) {
			appFrame.setVisible(false);
			menu.appFrame.setVisible(true);
		} else if (e.getSource() == jbCreate) {
			int row1 = jTable1.getSelectedRow();
			int row2 = jTable2.getSelectedRow();
			int idClient = (int)jTable1.getValueAt(row1, 0);
			int idProduct = (int)jTable2.getValueAt(row2, 0);
			try {
				oAdmin.createOrder(idClient, idProduct, Integer.parseInt(jtfQuantity.getText()));
			} catch (Exception b) {
				JOptionPane.showMessageDialog(appFrame,b.getMessage(), "Error!", JOptionPane.ERROR_MESSAGE);
			} finally {
				jtfQuantity.setText("");
				update();
				System.gc();
			}
		} else if (e.getSource() == jbEdit) {
			int row = jTable3.getSelectedRow();
			int columns = jTable3.getColumnCount();
			System.out.println((int)jTable3.getValueAt(row, 0));
			Order order = oAdmin.findOrderById((int)jTable3.getValueAt(row, 0));
			data = new Object[columns];
			for (int i = 1; i < columns; i++) {
				data[i] = jTable3.getValueAt(row, i);
				System.out.println(data[i]);
			}
			for (int i = 1; i < columns; i++) {
				System.out.println(jTable3.getColumnName(i));
				oAdmin.editOrder(order, jTable3.getColumnName(i), data[i]);
			}
			update();
			System.gc();
		} else if (e.getSource() == jbDelete) {
			int row = jTable3.getSelectedRow();
			System.out.println((int)jTable3.getValueAt(row, 0));
			Order order = oAdmin.findOrderById((int)jTable3.getValueAt(row, 0));
			oAdmin.deleteOrder(order);
			update();
			System.gc();
		} else if (e.getSource() == jbFindById) {
			Order order = oAdmin.findOrderById(Integer.parseInt(jtfFindById.getText()));
			for (int i = 0; i < jTable3.getRowCount(); i++) {
				if ((int)jTable3.getValueAt(i, 0) == order.getID()) {
					jTable3.setRowSelectionInterval(i, i);
				}
			}
			jtfFindById.setText("");
		} else if (e.getSource() == jbPrint) {
			int row = jTable3.getSelectedRow();
			int columns = jTable3.getColumnCount();
			System.out.println((int)jTable3.getValueAt(row, 0));
			Order order = oAdmin.findOrderById((int)jTable3.getValueAt(row, 0));
			data = new Object[columns];
			for (int i = 1; i < columns; i++) {
				data[i] = jTable3.getValueAt(row, i);
				System.out.println(data[i]);
			}
			Document document = new Document(PageSize.A4);
			document.addAuthor("Bogdan Pop");
			document.addTitle("Order no." + order.getID());
			try {
				PdfWriter.getInstance(document, new FileOutputStream("Order" + order.getID() + ".pdf"));
				document.open();
				document.add(new Paragraph("Order no." + order.getID()));
				for (int i = 1; i < columns; i++) {
					document.add(new Paragraph(jTable3.getColumnName(i) + ": " + data[i].toString()));
				}
				JOptionPane.showMessageDialog(null, "Order successfully printed!");
			} catch (Exception b) {
				b.printStackTrace();
			} finally {
				document.close();
			}
		}
	}

}
