package presentation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author pop_b
 * Parte a GUI
 * Clasa Meniu ne permite alegerea tabelei pe care dorim sa efectuam operatii
 */
public class Meniu implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JButton jbClient, jbProduct, jbOrder;
	ClientsView clView;
	ProductsView prView;
	OrdersView orView;
	
	public Meniu() {
		prepareGUI();
	}
	
	/**
	 * initializare Meniu
	 */
	private void prepareGUI() {
		appFrame = new JFrame("Meniu");
		appFrame.setSize(400, 200);
		appFrame.setLocation(700, 300);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	/**
	 * Aranjare componente din Meniu
	 */
	private void arrangeComponents() {
		// button pentru accesarea tabelului de clienti
		jbClient = new JButton("Clients");
		jbClient.setBounds(25, 75, 100, 50);
		jbClient.setFont(jbClient.getFont().deriveFont(15f));
		jbClient.addActionListener(this);
		cPane.add(jbClient);
		
		// button pentru accesarea tabelului de produse
		jbProduct = new JButton("Products");
		jbProduct.setBounds(150, 75, 100, 50);
		jbProduct.setFont(jbProduct.getFont().deriveFont(15f));
		jbProduct.addActionListener(this);
		cPane.add(jbProduct);
		
		// button pentru accesarea tabelului de comenzi
		jbOrder = new JButton("Orders");
		jbOrder.setBounds(275, 75, 100, 50);
		jbOrder.setFont(jbOrder.getFont().deriveFont(15f));
		jbOrder.addActionListener(this);
		cPane.add(jbOrder);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbClient) {
			clView = new ClientsView(this);
			appFrame.setVisible(false);
		} else if (e.getSource() == jbProduct) {
			prView = new ProductsView(this);
			appFrame.setVisible(false);
		} else if (e.getSource() == jbOrder) {
			orView = new OrdersView(this);
			appFrame.setVisible(false);
		}
	}
}
