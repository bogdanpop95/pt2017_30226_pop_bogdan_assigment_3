package model;

/**
 * @author pop_b
 * Clasa constructoare a unui obiect de tip Client, modelul fizic al clientului
 */
public class Client {
	
	private int idClient;			// unique ID
	private String name;		// Description
	private String phoneNumber;
	private String address;		// Client Addres
	private String email;		// email Address
	
	public Client() {
		
	}
	
	/**
	 * Constructor folosit la intoarcerea unui client din alte metode, findById
	 * @param iD	identificatorul unic al clientului
	 * @param name	Numele clientului
	 * @param phoneNumber	Numarul de telefon al clientului
	 * @param address	Adresa clientului
	 * @param email		Email-ul clientului
	 */
	public Client(int iD, String name, String phoneNumber, String address, String email) {
		super();
		idClient = iD;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.email = email;
	}
	
	/**
	 * Constructor folosit la creare, unde ID-ul clientului este generat automat de catre baza de date
	 * @param name	Numele clientului
	 * @param phoneNumber	Numarul de telefon al clientului
	 * @param address	Adresa clientului
	 * @param email		Email-ul clientului
	 */
	public Client(String name, String phoneNumber, String address, String email) {
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.email = email;
	}

	/**
	 * Setter-ele si getter-ele necesare
	 */
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int iD) {
		this.idClient = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
