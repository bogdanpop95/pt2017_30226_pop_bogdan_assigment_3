package model;

import java.util.*;

/**
 * @author pop_b
 * Clasa constructoare a unui obiect de tipul Order(Comanda), modelul fizic al unei comenzi
 * O comanda este compusa dintr-un obiect de tip Client si unul Product.
 */
public class Order {
	
	private int ID;
	private int idProduct;
	private String productName;
	private int idClient;
	private String clientName;
	private int desiredQuantity;
	private Date date;
	private double totalPrice;
	Product product;
	Client client;
	
	/**
	 * Constructor folosit la creare, unde ID-ul comenzii este generat automat de catre baza de date
	 * @param product	Obiect Product
	 * @param client	Obiect Client
	 * @param desiredQuantity	Cantitatea de produse dorita de catre client
	 * @param date	Data la care s-a facut comanda
	 */
	public Order(Product product, Client client, int desiredQuantity, Date date) {
		this.product = product;
		this.client = client;
		this.desiredQuantity = desiredQuantity;
		this.date = date;
		this.totalPrice = product.getPrice() * desiredQuantity;
		this.idProduct = product.getIdProduct();
		this.productName = product.getName();
		this.idClient = client.getIdClient();
		this.clientName = client.getName();
	}
	
	/**
	 * Constructor folosit la intoarcerea unei comenzi din alte metode, findById
	 * @param ID	identificatorul unic al comenzii
	 * @param product	Obiect Product
	 * @param client	Obiect Client
	 * @param desiredQuantity	Cantitatea de produse dorita de catre client
	 * @param date	Data la care s-a facut comanda
	 */
	public Order(int ID,Product product, Client client, int desiredQuantity, Date date) {
		this.ID = ID;
		this.product = product;
		this.client = client;
		this.desiredQuantity = desiredQuantity;
		this.date = date;
		this.totalPrice = product.getPrice() * desiredQuantity;
		this.idProduct = product.getIdProduct();
		this.productName = product.getName();
		this.idClient = client.getIdClient();
		this.clientName = client.getName();
	}
	/**
	 * Setter-ele si getter-ele necesare
	 */
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public int getDesiredQuantity() {
		return desiredQuantity;
	}
	public void setDesiredQuantity(int desiredQuantity) {
		this.desiredQuantity = desiredQuantity;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	
}
