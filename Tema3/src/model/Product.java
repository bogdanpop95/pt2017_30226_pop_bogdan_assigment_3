package model;

/**
 * @author pop_b
 * Clasa constructoare a unui obiect de tip produs, modelul fizic al produsului
 */
public class Product {
	
	private int idProduct;			// ID unique
	private String name;	// *Description*
	private double price;		// Price per unit
	private int quantity;	// Quantity left in stock
	
	public Product() {
		
	}
	
	/**
	 * Constructor folosit la intoarcerea unui client din alte metode, de ex. findById
	 * @param ID	identificatorul unic al produsului
	 * @param name	Numele produsului
	 * @param price	Pretul produsului
	 * @param quantity	Cantitatea disponibila in stoc
	 */
	public Product(int ID, String name, double price, int quantity) {
		super();
		this.idProduct = ID;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	
	/**
	 * Constructor folosit la creare, unde ID-ul produsului este generat automat de catre baza de date
	 * @param name	Numele produsului
	 * @param price	Pretul produsului
	 * @param quantity	Cantitatea disponibila in stoc
	 */
	public Product(String name, double price, int quantity) {
		super();
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	
	/**
	 * Setter-ele si getter-ele necesare
	 */
	public int getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(int iD) {
		this.idProduct = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
