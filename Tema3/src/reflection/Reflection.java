package reflection;

import java.util.*;
import java.lang.reflect.Field;


/**
 * @author pop_b
 * Cu ajutorul acestei clase putem extrage valorile atributelor unui obiect apartinand bineinteles unei clase
 * Ajuta la crearea JTable-urilor, datelor unei linii
 */
public class Reflection {
	
	public static List<Object> retrieveProprieties(Object object) {
		List<Object> values = new ArrayList<Object>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(object);
				values.add(value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return values;
	}

}
