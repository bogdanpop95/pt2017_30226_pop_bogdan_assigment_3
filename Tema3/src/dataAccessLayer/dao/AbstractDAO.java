package dataAccessLayer.dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import dataAccessLayer.connection.ConnectionFactory;
import reflection.Reflection;

/**
 * @author pop_b
 * Clasa abstracta care ne ajuta la crearea claselor DAO (Data Access Object) prin folosirea tipurilor generice (Generics) si a metodei de POO numita refelexivitate(*Refelection class)
 * @param <T> paramentrul de tip generic, care poate insemna un Client, Product, etc..
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/**
	 * Metoda creaza interogarea de tipul sugerat de numele metodei
	 * @param field numele coloanei dupa care se face interogarea, setand ulterior o valoare
	 * @return String, query-ul sub forma de String
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName().toLowerCase());
		if (!field.isEmpty())
			sb.append(" WHERE " + field + " = ?");
		return sb.toString();
	}
	
	/**
	 * Metoda creaza interogarea de tipul sugerat de numele metodei
	 * @param fields reprezinta o lista cu toate coloanele tabelei, inafara de cea cu id
	 * @return query-ul sub forma de String
	 */
	private String createInsertQuery(List<String> fields) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" (");
		for(int i = 0; i < fields.size(); i++) {;
			if (i != (fields.size() - 1))
				sb.append(fields.get(i) + ", ");
			else
				sb.append(fields.get(i) + ") "); 
		}
		sb.append("VALUES (");
		for(int i = 0; i < fields.size(); i++) {
			if (i != (fields.size() - 1))
				sb.append("?" + ", ");
			else
				sb.append("?" + ") "); 
		}
		return sb.toString();
	}
	
	/**
	 * Metoda creaza interogarea de tipul sugereat de numele metodei
	 * @param column coloana specifica ce camp al obiectului urmeaza sa fie modificat
	 * @param field selectia obiectului ce va fi modificat, din baza de date
	 * @return query-ul sub forma de String
	 */
	private String createUpdateQuery(String column, String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName().toLowerCase());
		sb.append(" SET " + column + " = ? ");
		sb.append("WHERE " + field + " = ?");
		return sb.toString();
	}
	
	/**
	 * Metoda creaza interogarea de tipul sugerat de numele metodei
	 * @param field	selectia obiectului ce va fi sters
	 * @return query-ul sub forma de String
	 */
	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM " + type.getSimpleName().toLowerCase());
		sb.append(" WHERE " + field + " = ?");
		return sb.toString();
	}

	/**
	 * Metoda care intoarce o lista cu toate obiectele din tabela specifica a bazei de date
	 * @return Lista de obiecte de tipul T (Client, Product)
	 */
	public List<T> findAll() {
		boolean flag = true;
		List<T> toReturn = null;
		Connection dbConnection = null;
		PreparedStatement findAllStatement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("");
		try {
			dbConnection = ConnectionFactory.getConnection();
			findAllStatement = dbConnection.prepareStatement(query);
			resultSet = findAllStatement.executeQuery();

			toReturn = createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
			flag = false;
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(dbConnection);
			if (flag)
				System.out.println(type.getName() + "DAO:findAll " + "completed successfully!");
			else
				System.out.println(type.getName() + "DAO:findAll " + "failed!");
		}
		return toReturn;
	}

	/**
	 * Metoda intoarce un obiect din tabel prin specificarea identificatorului unic
	 * @param id identificatorul unic al obiectului
	 * @return obiectul
	 */
	public T findById(int id) {
		T toReturn = null;
		Connection dbConnection = null;
		PreparedStatement findStatement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id" + type.getSimpleName());
		try {
			dbConnection = ConnectionFactory.getConnection();
			findStatement = dbConnection.prepareStatement(query);
			findStatement.setInt(1, id);
			resultSet = findStatement.executeQuery();

			toReturn = createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
			if (toReturn != null)
				System.out.println("SELECT operation on " + type.getSimpleName() + " with id" + type.getSimpleName() + " =  " + (int)Reflection.retrieveProprieties(toReturn).get(0) + " completed successfully!");
		}
		return toReturn;
	}
	

	/**
	 * Metoda insereaza in baza de date, obiectul t
	 * @param t obiectul de tip T
	 * @return identificatorul obiectului creat, introdus in tabela
	 */
	public int insert(T t) {
		Connection dbConnection = null;
		PreparedStatement insertStatement = null;
		ResultSet resultSet = null;
		List<String> fields = getFields(t);
		String query = createInsertQuery(fields);
		int insertedID = -1;
		try {
			dbConnection = ConnectionFactory.getConnection();
			insertStatement = dbConnection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			List<Object> values = Reflection.retrieveProprieties(t);
			int i = 1;
			for (Object obj : values) {
				if (values.indexOf(obj) != 0) {
					insertStatement.setString(i++, obj.toString());
				}
			}
			insertStatement.executeUpdate();
			resultSet = insertStatement.getGeneratedKeys();
			if (resultSet.next()) {
				insertedID = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
			if (insertedID > -1)
				System.out.println("INSERT operation on " + type.getSimpleName() + " with id" + type.getSimpleName() + " =  " + (int)Reflection.retrieveProprieties(t).get(0) +" completed successfully!");
			else
				System.out.println("INSERT operation on " + type.getSimpleName() + " with id" + type.getSimpleName() + " =  " + (int)Reflection.retrieveProprieties(t).get(0) +" failed!");
		}
		return insertedID;
	}

	/**
	 * Metoda editeaza (update) la un obiect din tabela
	 * @param t obiectul pe care urmeaza sa il modificam
	 * @param columnName numele atributului care se doreste a fi modificat
	 * @param newValue noua valoare a atributului
	 */
	public void edit(T t, String columnName, Object newValue) {
		boolean flag = true;
		Connection dbConnection = null;
		PreparedStatement editStatement = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery(columnName, "id" + type.getSimpleName());
		try {
			dbConnection = ConnectionFactory.getConnection();
			editStatement = dbConnection.prepareStatement(query);
			editStatement.setString(1, newValue.toString());
			editStatement.setInt(2, (int)Reflection.retrieveProprieties(t).get(0));
			editStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:edit " + e.getMessage());
			flag = false;
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(editStatement);
			ConnectionFactory.close(dbConnection);
			if (flag)
				System.out.println("type.getName()" + "DAO:edit" + "completed successfully!");
			else
				System.out.println("type.getName()" + "DAO:edit " + "failed!");
		}	
	}
	
	/**
	 * Metoda permite stergea din tabela a unui obiect t
	 * @param t obiectul de sters
	 */
	public void delete(T t) {
		boolean flag = true;
		Connection dbConnection = null;
		PreparedStatement deleteStatement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery("id" + type.getSimpleName());
		try {
			dbConnection = ConnectionFactory.getConnection();
			deleteStatement = dbConnection.prepareStatement(query);
			deleteStatement.setInt(1, (int)Reflection.retrieveProprieties(t).get(0));
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
			flag = false;
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			if (flag)
				System.out.println("DELETE operation on " + type.getSimpleName() + " with id" + type.getSimpleName() + " =  " + (int)Reflection.retrieveProprieties(t).get(0) +" completed successfully!");
			else
				System.out.println("DELETE operation on " + type.getSimpleName() + " with id" + type.getSimpleName() + " =  " + (int)Reflection.retrieveProprieties(t).get(0) +" failed!");
		}
	}

	/**
	 * Metoda care inglobeaza reflexivitatea si care creaza o lista de obiecte, rezultate in urma unei interogari
	 * @param resultSet lista de obiecte din tabel generata de interogare
	 * @return lista de obiecte generata
	 */
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Metoda ne ajuta, tot prin reflexivitate sa obtinem atributele unui obiect de tipul T; Folosita la inserare
	 * @param t obiectul clasei T
	 * @return lista de atribute
	 */
	private List<String> getFields(T t) {
		List<String> fields = new ArrayList<String>();
		for (Field field : t.getClass().getDeclaredFields()) {
			if (!field.equals(t.getClass().getDeclaredFields()[0])) {
				fields.add(field.getName());
			}

		}
		return fields;
	}

}
