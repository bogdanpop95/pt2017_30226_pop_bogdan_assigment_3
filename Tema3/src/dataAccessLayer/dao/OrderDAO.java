package dataAccessLayer.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import dataAccessLayer.connection.ConnectionFactory;

import java.util.*;

import model.Order;

/**
 * @author pop_b
 * Clasa care inglobeaza operatiile pe comenzi, similara cu ClientDAO, ProductDAO dar care implementeaza direct operatiile fara mostenire (o alta abordare, mai simpla in cazul de fata)
 */
public class OrderDAO {
	public static ClientDAO cDAO;
	public static ProductDAO pDAO;
	
	
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO orders (idProduct, productName, idClient, clientName, quantity, dated, totalPrice)"
													  + " VALUES (?, ?, ?, ?, ?, ?, ?)";
	private static String editStatementString;
	private final static String deleteStatementString = "DELETE FROM orders" + " WHERE idOrder = ?";
	private final static String findStatementString = "SELECT *" + " FROM orders" + " WHERE idOrder = ?";
	private final static String findAllStatementString = "SELECT *" + " FROM orders";
	
	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement insertStatement = null;
		int insertedId = -1;
		ResultSet rs = null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getProduct().getIdProduct());
			insertStatement.setString(2, order.getProduct().getName());
			insertStatement.setInt(3, order.getClient().getIdClient());
			insertStatement.setString(4, order.getClient().getName());
			insertStatement.setInt(5, order.getDesiredQuantity());
			insertStatement.setDate(6, new java.sql.Date(order.getDate().getTime()));
			insertStatement.setDouble(7, order.getTotalPrice());
			insertStatement.executeUpdate();
			
			rs = insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
			if (insertedId > -1)
				System.out.println("INSERT operation on Order(idOrder = " + order.getID() + ") completed successfully!");
			else
				System.out.println("INSERT operation on Order(idOrder =" + order.getID()  + " ) failed!");
		}
		return insertedId;
	}
	
	public static void edit(Order order, String columnName, Object newValue) {
		boolean flag = true;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement editStatement = null;
		editStatementString = "UPDATE orders " + "SET " + columnName + " = ?" + " WHERE idOrder = ?";
		try {
			editStatement = dbConnection.prepareStatement(editStatementString);
			editStatement.setString(1, newValue.toString());
			editStatement.setInt(2, order.getID());
			editStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:edit " + e.getMessage());
			flag = false;
		} finally {
			ConnectionFactory.close(editStatement);
			ConnectionFactory.close(dbConnection);
			if (flag)
				System.out.println("EDIT operation on Order(idOrder = " + order.getID() + ") completed successfully!");
			else
				System.out.println("EDIT operation on Order(idOrder =" + order.getID()  + " ) failed!");
		}
	}
	
	public static int delete(Order order) {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		int rs = -1;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, order.getID());
			rs = deleteStatement.executeUpdate();
			if (rs == -1) {
				System.out.println("DELETE operation on Order(idOrder =" + order.getID()  + " ) failed!");
				return -1;
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			System.out.println("DELETE operation on Order(idOrder = " + order.getID() + ") completed successfully!");
		}
		return rs;
	}
	
	public static Order findById(int idOrder) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idOrder);
			rs = findStatement.executeQuery();
			if(rs.next()) {
				int idProduct = rs.getInt("idProduct");
				int idClient = rs.getInt("idClient");
				int quantity = rs.getInt("quantity");
				Date date = rs.getDate("dated");
				toReturn = new Order(idOrder, pDAO.findById(idProduct),cDAO.findById(idClient), quantity, date);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
			if (toReturn != null)
				System.out.println("SELECT operation on Order with idOrder =  " + toReturn.getID() + ") completed successfully!");
		}
		return toReturn;
	}
	
	public static List<Order> findAll() {
		boolean flag = true;
		List<Order> orders = new ArrayList<Order>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findAllStatement = null;
		ResultSet rs = null;
		try {
			findAllStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findAllStatement.executeQuery();
			while (rs.next()) {
				int idOrder = rs.getInt("idOrder");
				int idProduct = rs.getInt("idProduct");
				int idClient = rs.getInt("idClient");
				int quantity = rs.getInt("quantity");
				Date date = rs.getDate("dated");
				orders.add(new Order(idOrder, pDAO.findById(idProduct),cDAO.findById(idClient), quantity, date));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findAll " + e.getMessage());
			flag = false;
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findAllStatement);
			ConnectionFactory.close(dbConnection);
			if (flag)
				System.out.println("SELECT * operation completed successfully!");
			else
				System.out.println("SELECT * operation failed!");
		}
		return orders;
	}
}
