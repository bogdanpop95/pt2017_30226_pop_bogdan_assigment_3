package dataAccessLayer.dao;

import java.util.*;

import model.Product;

/**
 * @author pop_b
 * Clasa care inglobeaza operatiile pe Produse si mosteneste clasa AbstracDAO
 */
public class ProductDAO extends AbstractDAO<Product>{
	
	public ProductDAO() {
		super();
	}
	
	public List<Product> findAll() {
		return super.findAll();
	}
	
	public Product findById(int idProduct) {
		return super.findById(idProduct);
	}
	
	public int insert(Product product) {
		return super.insert(product);
	}
	
	public void edit(Product product, String columnName, Object newValue) {
		super.edit(product, columnName, newValue);
	}
	
	public void delete(Product product) {
		super.delete(product);
	}
}
