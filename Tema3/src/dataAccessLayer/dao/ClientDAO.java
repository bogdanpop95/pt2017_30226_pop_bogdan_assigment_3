package dataAccessLayer.dao;

import java.util.*;

import model.Client;

/**
 * @author pop_b
 * Clasa care inglobeaza operatiile pe Clienti si care mosteneste clasa AbstracDAO
 */
public class ClientDAO extends AbstractDAO<Client>{
	
	public ClientDAO() {
		super();
	}
	
	public List<Client> findAll() {
		return super.findAll();
	}
	
	public Client findById(int idClient) {
		return super.findById(idClient);
	}
	
	public int insert(Client client) {
		return super.insert(client);
	}
	
	public void edit(Client client, String columnName, Object newValue) {
		super.edit(client, columnName, newValue);
	}
	
	public void delete(Client client) {
		super.delete(client);
	}

}
